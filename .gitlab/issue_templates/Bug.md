### Summary

<!--
Summarize the bug and how it can be reproduced. If necessary, include a link to an example project that exhibits the behavior.
-->

### Environment

- Application version:
- Operating system:
- Vale version:

/label ~Bug
