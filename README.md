# Vale rules for GitLab CI Utils

This repository contains a [Vale](https://vale.sh/) package with a readability rule and
vocabulary for projects in the
[GitLab CI Utils](https://gitlab.com/gitlab-ci-utils/vale-package-gciu) group.

The rules are vocabulary in this package aren't included by default and must be
explicitly included in a Vale configuration file as shown:

`.vale.ini`:

```ini
StylesPath = .gitlab/vale/styles
# Specify that the GCIU package v1.0.1 (or other appropriate version) should be
# used. The link can be found on the package's releases page at
# https://gitlab.com/gitlab-ci-utils/vale-package-gciu/-/releases.
Packages = Google, \
  https://gitlab.com/gitlab-ci-utils/vale-package-gciu/-/releases/1.0.1/downloads/GCIU.zip
# Specify that the GCIU vocabulary should be used
Vocab = GCIU

[*]
# Specify that the GCIU rules should be included for all file types
BasedOnStyles = Vale, Google, GCIU
```
