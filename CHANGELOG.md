# Changelog

## v1.0.6 (2025-01-04)

### Fixed

- Updated vocabulary based on new projects. (#6)

## v1.0.5 (2024-10-13)

### Fixed

- Added "SHA" to GCIU vocabulary. (#5)

## v1.0.4 (2024-06-19)

### Miscellaneous

- Updated CI pipeline `lint_prose` job to test rules from repository, not the
  latest published. (#4)
- Updated Renovate config with missing preset to manage Vale packages.

## v1.0.3 (2024-06-19)

### Fixed

- Added new project names to vocabulary. Also added a handful of missing terms
  from a review of all projects. (#3)
- Fixed permission issue preventing Renovate from running.
- Updated GitLab Releaser file for breaking changes in v8 and GitLab 17.0.

### Miscellaneous

- Updated Renovate config to use use new [presets](https://gitlab.com/gitlab-ci-utils/renovate-config)
  project. (#2)

## v1.0.2 (2024-01-22)

### Fixed

- Updated vocabulary with inputs from initial scan of all projects. (#1)

## v1.0.1 (2024-01-13)

### Fixed

- Fixed release to create permalinks to the package registry.

## v1.0.0 (2024-01-13)

Initial release
